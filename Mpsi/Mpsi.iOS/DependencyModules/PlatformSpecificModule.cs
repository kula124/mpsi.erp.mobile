﻿using Autofac;
using Mpsi.Mobile.Core.Services.Interfaces;
using Mpsi.Mobile.iOS.Services;


namespace Mpsi.Mobile.iOS.DependencyModules
{
    public class PlatformSpecificModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<LoggerService>()
                .As<ILogger>()
                .SingleInstance();


            builder.RegisterType<Localize>()
                .As<ILocalize>()
                .SingleInstance();


            // Local services
        }
    }
}
