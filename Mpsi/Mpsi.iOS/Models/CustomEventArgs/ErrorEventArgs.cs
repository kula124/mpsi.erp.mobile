﻿using System;

namespace Mpsi.Mobile.iOS.Models.CustomEventArgs
{
    public class ErrorEventArgs : EventArgs
    {
        public bool HasHttpErrors { get; }

        public int HttpStatusCode { get; }

        public string ErrorMessage { get; }

        public ErrorEventArgs(int httpStatusCode, string errorMessage, bool hasHttpErrors)
        {
            HttpStatusCode = httpStatusCode;
            ErrorMessage = errorMessage;
            HasHttpErrors = hasHttpErrors;
        }

        public ErrorEventArgs(string errorMessage)
        {
            ErrorMessage = errorMessage;
            HasHttpErrors = true;
        }
    }
}