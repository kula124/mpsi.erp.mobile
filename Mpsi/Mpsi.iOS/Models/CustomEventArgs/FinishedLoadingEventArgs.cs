﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Mpsi.Mobile.iOS.Models.CustomEventArgs
{
    public class FinishedLoadingEventArgs : EventArgs
    {
        public FinishedLoadingEventArgs(string data, bool hasHttpErrors, List<Cookie> cookies)
        {
            Data = data;
            
            HasHttpErrors = hasHttpErrors;
            Cookies = cookies;
        }

        public string Data { get; }
        
        public bool HasHttpErrors { get; }

        public List<Cookie> Cookies { get; }
    }
}