﻿using Autofac;
using System;
using Autofac.Core;
using Prism.Autofac.Forms;
using Mpsi.Mobile.Core.DependencyModules;
using Mpsi.Mobile.Core.Resources;
using Mpsi.Mobile.Core.Services.Interfaces;
using Mpsi.Mobile.Core.ViewModels;
using Mpsi.Mobile.Core.Views;
using Xamarin.Forms;
using System;

namespace Mpsi.Mobile.Core
{
    public partial class App
    {
        public App(IModule[] platformSpecificModules, IPlatformInitializer initializer = null) : base(initializer)
        {
            PrepareContainer(platformSpecificModules);

            InitializeComponent();
            SetCultureAndLanguage();
            NavigationService.NavigateAsync(nameof(LoginPage));
            
        }

        

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<LoginPage, AuthenticationViewModel>();
            Container.RegisterTypeForNavigation<MainTabbedPage, MainTabbedViewModel>();
            Container.RegisterTypeForNavigation<NavigationPage>();

            //Container.RegisterTypeForNavigation<AgreementListPage>();
            //Container.RegisterTypeForNavigation<AgreementReadPage>();
            //Container.RegisterTypeForNavigation<LineItemDataEditPage>();

            //Container.RegisterTypeForNavigation<UserDetailsPage>();
            //Container.RegisterTypeForNavigation<DiscountGuidelinePage, DiscountGuidelinePageViewModel>();
            //Container.RegisterTypeForNavigation<UserCertificatePinInputModalPage, UserCertificatePinInputViewModel>();
            //Container.RegisterTypeForNavigation<DiscountGuideLineGraphPage, DiscountGuideLineGraphViewModel>();
            //Container.RegisterTypeForNavigation<TestIntranetPage, TestIntranetViewModel>();

        }

        private void PrepareContainer(IModule[] platformSpecificModules)
        {
            // Build the container.
            var builder = new ContainerBuilder();
            builder.RegisterModule<MpsiMobileModule>();
            builder.RegisterModule<ServicesModule>();
            builder.RegisterModule<HttpCommunicationModule>();

            // RegisterPlatformSpecificModules
            foreach (var platformSpecificModule in platformSpecificModules)
            {
                builder.RegisterModule(platformSpecificModule);
            }

            builder.Update(Container);
        }

        private void SetCultureAndLanguage()
        {
            if (Device.RuntimePlatform != Device.iOS && Device.RuntimePlatform != Device.Android) return;

            var dependencyService = Container.Resolve<ILocalize>();
            var ci = dependencyService.GetCurrentCultureInfo();
            AppResources.Culture = ci; // set the RESX for resource localization
            dependencyService.SetLocale(ci); // set the Thread for locale-aware methods
        }
    }
}
