﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Mpsi.Mobile.Core.Resources;
using Mpsi.Mobile.Core.Models;
using Mpsi.Mobile.Core.Models.Base;
using Mpsi.Mobile.Core.Models.Exceptions;
using Mpsi.Mobile.Core.Services.Interfaces;

namespace Mpsi.Mobile.Core.Services
{
    public sealed class Resiliency : IResiliency
    {
        private IDialogService DialogService { get; }
        private readonly JsonSerializerSettings _serializerSettings;

        public Resiliency(IDialogService dialogService)
        {
            DialogService = dialogService;

            _serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                NullValueHandling = NullValueHandling.Ignore,

            };
        }

        public void Try(Action action) => Try(action, () => { });

        public void Try(Action action, Action finallyAction)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));

            try
            {
                action();
            }
            catch (ServiceAuthenticationException serviceAuthenticationException)
            {
                Task.FromResult(ServiceAuthenticationExceptionAction(serviceAuthenticationException));
            }
            catch (CustomHttpRequestException customHttpRequestException)
            {
                Task.FromResult(CustomHttpRequestExceptionAction(customHttpRequestException));
            }
            catch (HttpRequestException httpRequestException)
            {
                Task.FromResult(HttpRequestExceptionAction(httpRequestException));
            }
            catch (JsonSerializationException jsonSerializationException)
            {
                Task.FromResult(JsonSerializationExceptionAction(jsonSerializationException));
            }
            catch (Exception ex)
            {
                Task.FromResult(ExceptionAction(ex));
            }
            finally
            {
                finallyAction();
            }
        }

        public async Task TryAsync(Func<Task> action) => await TryAsync(action, () => { });

        public async Task TryAsync(Wrapper<bool> isBusy, Func<Task> action) =>
            await TryAsync(isBusy, action, () => { });

        public async Task TryAsync(Func<Task> action, Action finallyAction)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));

            try
            {
                await action();
            }
            catch (ServiceAuthenticationException serviceAuthenticationException)
            {
                await ServiceAuthenticationExceptionAction(serviceAuthenticationException);
            }
            catch (CustomHttpRequestException customHttpRequestException)
            {
                await CustomHttpRequestExceptionAction(customHttpRequestException);
            }
            catch (HttpRequestException httpRequestException)
            {
                await HttpRequestExceptionAction(httpRequestException);
            }
            catch (JsonSerializationException jsonSerializationException)
            {
                await JsonSerializationExceptionAction(jsonSerializationException);
            }
            catch (Exception ex)
            {
                await ExceptionAction(ex);
            }
            finally
            {
                finallyAction();
            }
        }

        public async Task TryAsync(Wrapper<bool> isBusy, Func<Task> action, Action finallyAction)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));

            if (isBusy.Value) return;

            try
            {
                isBusy.Value = true;

                await action();
            }
            catch (ServiceAuthenticationException serviceAuthenticationException)
            {
                await ServiceAuthenticationExceptionAction(serviceAuthenticationException);
            }
            catch (CustomHttpRequestException customHttpRequestException)
            {
                await CustomHttpRequestExceptionAction(customHttpRequestException);
            }
            catch (HttpRequestException httpRequestException)
            {
                await HttpRequestExceptionAction(httpRequestException);
            }
            catch (JsonSerializationException jsonSerializationException)
            {
                await JsonSerializationExceptionAction(jsonSerializationException);
            }
            catch (Exception ex)
            {
                await ExceptionAction(ex);
            }
            finally
            {
                finallyAction();

                isBusy.Value = false;
            }
        }

        private async Task ServiceAuthenticationExceptionAction(
            ServiceAuthenticationException serviceAuthenticationException)
        {
            await DialogService.ShowAlertAsync(serviceAuthenticationException.Content,
                AppResources.CommonAuthenticationError, AppResources.CommonClose);
        }

        private async Task CustomHttpRequestExceptionAction(CustomHttpRequestException customHttpRequestException)
        {
            await DialogService.ShowAlertAsync($"{customHttpRequestException.Message}",
                $"{AppResources.CommonError} {customHttpRequestException.Status}", AppResources.CommonClose);
        }

        private async Task HttpRequestExceptionAction(HttpRequestException httpRequestException)
        {
           
            await DialogService.ShowExceptionAlertAsync($"{httpRequestException.Message}");
        }

        private async Task JsonSerializationExceptionAction(JsonSerializationException jsonSerializationException)
        {
            await DialogService.ShowExceptionAlertAsync(jsonSerializationException.Message);
        }

        private async Task ExceptionAction(Exception exception)
        {
            await DialogService.ShowExceptionAlertAsync(exception.Message);
        }

        private BusinessSideHttpRequestExceptionMessageModel Parse(string content)
        {
            try
            {
                return JsonConvert.DeserializeObject<BusinessSideHttpRequestExceptionMessageModel>(content, _serializerSettings);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}