﻿namespace Mpsi.Mobile.Core.Services.Interfaces
{
    public interface IViewModelAggregateService
    {


        IDialogService DialogService { get; }

        IResiliency Resiliency { get; }
    }
}
