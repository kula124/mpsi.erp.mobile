﻿using System.Globalization;

namespace Mpsi.Mobile.Core.Services.Interfaces
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
        void SetLocale(CultureInfo ci);
    }
}
