﻿using System.Collections.Generic;

namespace Mpsi.Mobile.Core.Services.Interfaces
{
    /// <summary>
    /// Holds a collection of objects by their keys.
    /// </summary>
    public interface IDataRegion
    {
        /// <summary>
        /// Dictionary holding all the regions of the cache.
        /// </summary>
        IDictionary<string, object> Objects { get; set; }

        /// <summary>
        /// Name of the region.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Deletes all objects in the region.
        /// </summary>
        void Clear();

        /// <summary>
        /// Adds an object to the region in the cache.
        /// </summary>
        /// <param name="key">A unique value that is used to store and retrieve the object from the cache.</param>
        /// <param name="value">The object saved to the cache.</param>
        void Add(string key, object value);

        /// <summary>
        /// Removes an object from the region in the cache.
        /// </summary>
        /// <param name="key">A unique value that is used to store and retrieve the object from the cache.</param>
        void Remove(string key);

        /// <summary>
        /// Gets an object from the region in the cache by using the specified key.
        /// </summary>
        /// <param name="key">The unique value that is used to identify the object in the cache.</param>
        /// <returns>The object that was cached by using the specified key. Null is returned if the key does not exist.</returns>
        object Get(string key);

        /// <summary>
        /// Gets an enumerable list of all cached objects in the region.
        /// </summary>
        /// <returns>An enumerable list of all cached objects in the region.</returns>
        IEnumerable<KeyValuePair<string, object>> GetObjects();
    }
}