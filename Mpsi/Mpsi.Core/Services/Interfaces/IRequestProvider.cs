﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mpsi.Mobile.Core.Services.Interfaces
{
    public interface IRequestProvider
    {
        Task<TResult> GetAsync<TResult>(string path);

        Task<TResult> PutAsync<TResult>(string uri, string data);

        //Task<TResult> PostAsync<TResult>(string uri, TResult data, string token = "", string header = "");

        Task<TResult> PostAsync<TResult>(string uri, string data);

        Task<TResult> PostAsync<TResult>(string uri, List<KeyValuePair<string, string>> formParameters,
            List<KeyValuePair<string, string>> headerParameters);

        //Task DeleteAsync(string uri, string token = "");
    }
}
