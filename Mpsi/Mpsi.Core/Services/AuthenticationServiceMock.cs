﻿using System;
using System.Threading.Tasks;
using Mpsi.Mobile.Core.Models.Api.Requests;
using Mpsi.Mobile.Core.Models.Api.Responses;
using Mpsi.Mobile.Core.Models.Enums;
using Mpsi.Mobile.Core.Services.Interfaces;

namespace Mpsi.Mobile.Core.Services
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class AuthenticationServiceMock : IAuthenticationService
    {
        public Task<LoginPostResponse> LoginUser(LoginPostRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
