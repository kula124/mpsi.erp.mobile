﻿using System;
using System.Globalization;
using Mpsi.Mobile.Core.Utility.Extensions;
using Xamarin.Forms;

namespace Mpsi.Mobile.Core.Utility.Converters
{
    public class ObjectToBoolConverter<T> : IValueConverter
    {
        public T FalseObject { set; get; }

        public T TrueObject { set; get; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return FalseObject;

            if (value is string) return string.IsNullOrWhiteSpace(value as string) ? FalseObject : TrueObject;

            if (value.IsNumber()) return value.IsDefaultValueOfNumber() ? FalseObject : TrueObject;

            return TrueObject;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
