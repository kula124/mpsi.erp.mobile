﻿namespace Mpsi.Mobile.Core.Models.NavigationParameters
{
    public abstract class BaseNavigationParametersModel
    {
        public BaseNavigationParametersModel()
        {
            
        }

        public abstract Prism.Navigation.NavigationParameters ToNavigationParameters();
    }
}
