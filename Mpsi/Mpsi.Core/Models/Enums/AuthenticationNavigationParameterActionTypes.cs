﻿namespace Mpsi.Mobile.Core.Models.Enums
{
    public enum AuthenticationNavigationParameterActionTypes
    {
        None = 0,
        Logout,
        ImportCertificates
    }
}
