﻿namespace Mpsi.Mobile.Core.Models.Base
{
    public class SelectableData<T> : ModelBase
    {
        private T _data;
        private bool _isSelected;
        private bool _canSelect;

        public T Data
        {
            get { return _data; }
            set
            {
                _data = value;
                OnPropertyChanged();
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public bool CanSelect
        {
            get { return _canSelect; }
            set
            {
                _canSelect = value;
                OnPropertyChanged();
            }
        }
    }
}
