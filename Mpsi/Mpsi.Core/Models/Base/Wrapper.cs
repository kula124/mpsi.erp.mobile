﻿namespace Mpsi.Mobile.Core.Models.Base
{
    public class Wrapper<T> : ModelBase
    {
        private T _value;

        public T Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged();
            }
        }
    }
}
