using System.Collections.Generic;
using Newtonsoft.Json;

namespace Mpsi.Mobile.Core.Models.Api.Responses
{
    public abstract class BaseGetResponse<TContent>
    {
        [JsonProperty("data")]
        public TContent Data { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }

        [JsonProperty("filter")]
        public object Filter { get; set; }
    }
}