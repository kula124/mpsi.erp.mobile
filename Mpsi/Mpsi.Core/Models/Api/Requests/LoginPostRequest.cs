﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mpsi.Mobile.Core.Models.Api.Requests.Base;

namespace Mpsi.Mobile.Core.Models.Api.Requests
{
    public class LoginPostRequest : BasePostParametersRequest
    {

        public override string GetRelativeUriPath() => GlobalSettings.Instance.LoginGetRelativePath;

        
    }
}
