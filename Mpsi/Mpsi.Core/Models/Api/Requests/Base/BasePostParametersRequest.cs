﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mpsi.Mobile.Core.Models.Api.Requests.Base
{
    public abstract class BasePostParametersRequest: BaseRequest
    {
        public override string GetRelativeUriPath() => GlobalSettings.Instance.LoginGetRelativePath;

        public string GrantTypeKey { get; } = "grant_type";

        public string GrantTypeValue { get; } = "password";

        public string ClientIdKey { get; } = "client_id";

        public string ClientIdValue { get; } = "mpsi_mobile";

        public string DeviceSerialNumberKey { get; } = "device_serial_number";

        public string DeviceSerialNumberValue { get; } = "MC-1";

        public string UserNameKey { get; } = "username";

        public string PasswordKey { get; } = "password";

        public string PasswordValue { get; set; }

        public string UserNameValue { get; set; }

        public string ContentTypeKey { get; } = "Content-Type";

        public string ContentTypeValue { get; } = "application/x-www-form-urlencoded";
    }
}
