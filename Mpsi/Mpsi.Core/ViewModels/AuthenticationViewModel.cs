﻿using System.Threading.Tasks;
using System.Windows.Input;
using Mpsi.Mobile.Core.Helpers;
using Mpsi.Mobile.Core.Models.Api.Requests;
using Prism.Navigation;
using Xamarin.Forms;
using Mpsi.Mobile.Core.Services.Interfaces;
using Mpsi.Mobile.Core.ViewModels.Base;

namespace Mpsi.Mobile.Core.ViewModels
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class AuthenticationViewModel : ViewModelBase
    {

        private string _userName;
        private string _password;

        private readonly IAuthenticationService _authenticationService;


        public AuthenticationViewModel(IViewModelAggregateService aggregateService,
            INavigationService navigationService, IAuthenticationService authenticationService) : base(aggregateService, navigationService)
        {
            _authenticationService = authenticationService;

        }

        public ICommand LoginCommand => new Command(async () =>
        {
            await Resiliency.TryAsync(IsBusy, async () =>
            {
                await LoginAsync();
            });
            
        });

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; RaisePropertyChanged(); }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; RaisePropertyChanged(); }
        }


        private async Task LoginAsync()
        {

            var request = new LoginPostRequest {UserNameValue = UserName, PasswordValue = Password};

            var loginResponse = await _authenticationService.LoginUser(request);

            GlobalSettings.Instance.AccessToken = loginResponse.AccessToken;
            GlobalSettings.Instance.RefreshToken = loginResponse.RefreshToken;
            //Settings.UserName = UserName;
            //Settings.Password = Password;

            //TODO: Napravi idući screen i odi se na njega npr:
            //await NavigationService.NavigateAsync(
            //            $"{nameof(MainTabbedPage)}/{nameof(NavigationPage)}/{nameof(AgreementListPage)}");


            //    await DialogService.ShowExceptionAlertAsync("Login for certificate you provided was unsuccessful.");
            
            
        }

    }
}
