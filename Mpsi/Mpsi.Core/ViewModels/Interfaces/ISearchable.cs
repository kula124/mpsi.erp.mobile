﻿namespace Mpsi.Mobile.Core.ViewModels.Interfaces
{
    public interface ISearchable
    {
        void Search();
    }
}
