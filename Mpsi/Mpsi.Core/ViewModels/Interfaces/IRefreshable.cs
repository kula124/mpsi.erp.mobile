﻿using System.Threading.Tasks;

namespace Mpsi.Mobile.Core.ViewModels.Interfaces
{
    public interface IRefreshable
    {
        Task RefreshAsync();
    }
}
