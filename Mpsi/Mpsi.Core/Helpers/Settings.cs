﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Mpsi.Mobile.Core.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings => CrossSettings.Current;

        #region Setting Constants

        
        private const string RefreshTokenKey = "RefreshTokenKey";
        private static readonly string RefreshTokenKeyDefault = string.Empty;

        private const string AccessTokenKey = "AccessTokenKey";
        private static readonly string AccessTokenDefault = string.Empty;


        #endregion


        public static string AccessToken
        {
            get { return AppSettings.GetValueOrDefault<string>(AccessTokenKey, AccessTokenDefault); }
            set { AppSettings.AddOrUpdateValue<string>(AccessTokenDefault, value); }
        }

        public static string RefreshToken
        {
            get { return AppSettings.GetValueOrDefault<string>(RefreshTokenKey, RefreshTokenKeyDefault); }
            set { AppSettings.AddOrUpdateValue<string>(RefreshTokenKeyDefault, value); }
        }

       

    }
}
