﻿using System;
using System.Net;
using Mpsi.Mobile.Core.Models.Enums;

namespace Mpsi.Mobile.Core
{
    public class GlobalSettings
    {

        private readonly string _defaultEndpoint = "http://mpsi.celebesit.hr/mobile/";
        //private readonly string _defaultEndpointProduction = "https://pricing-light.siemens.com/api/";

        private static readonly GlobalSettings _instance = new GlobalSettings();

        

        public static string LoginIndex => "2";

        public static GlobalSettings Instance => _instance;

        public string BaseEndpoint => _defaultEndpoint;

        public string LoginUrlProduction =>
            $"";

        public string LoginUrlTest =>
            $"";

        public string ClientGid { get; set; } = "";

        public string ClientRegion { get; set; } = "";

        public string ClientToken { get; set; } = "";

        public Cookie JSessionCookie { get; set; }

        //public Cookie AuthSessionCookie { get; set; }



        #region Relative Paths


        public string LoginGetRelativePath { get; } = "api/connect/token";

        #endregion



        public  string AccessToken { get; set; }
        public  string RefreshToken { get; set; }


    }
}
