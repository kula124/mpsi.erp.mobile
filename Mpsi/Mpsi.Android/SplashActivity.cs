﻿using Android.App;
using Android.OS;

namespace Mpsi.Mobile.Android
{
    [Activity(Theme = "@style/Theme.Splash", //Indicates the theme to use for this activity
        MainLauncher = false, //Set it as boot activity
        NoHistory = true)] //Doesn't place it in back stack
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {

            //ServicePointManager.MaxServicePointIdleTime = 0;
            //ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) =>
            //{
            //    return true;
            //};


            base.OnCreate(bundle);
            System.Threading.Thread.Sleep(3000); //Let's wait awhile...
            this.StartActivity(typeof(MainActivity));
        }
    }
}