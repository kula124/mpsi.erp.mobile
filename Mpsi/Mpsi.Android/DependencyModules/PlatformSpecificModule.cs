﻿using Autofac;
using Mpsi.Mobile.Core.Services.Interfaces;


namespace Mpsi.Mobile.Android.DependencyModules
{
    public class PlatformSpecificModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<Localize>()
                .As<ILocalize>()
                .SingleInstance();
        }
    }
}